# DIPZ Training

This is where the training of DIPZ is documented and done.

## Contents:
1. **Configs**: where the json files that configure the training parameters exist
2. **Notebooks**: where the jupyter notebooks for training/validation of DIPZ exist, as well as their python helper functions
3. **Old_Training_Outputs**: where the architecture and the weights of the old DIPZ training is saved
4. **Outputs**: where the output folders resulting from the trainings will be saved
5. **SMU_M3_Scripts**: where the submission scripts for running the training on the SMU GPUs exit

##  How to train DIPZ yourself:
1. Set up the conda environment needed for DIPZ training (Skip this if you've done so already for a different purpose):
The DIPZ conda environment is saved in the root directory in The DIPZ Project gitlab repository in a file called ```dipz_environment.yml```. You should be able to re-create the environment from this directory by executing the command:
```
conda env create -f ../dipz_environment.yml
```
**In case you're running on the SMU M3 cluster:** You should do ```module load conda``` before creating the environment to load conda into your M3 session

2. Configure your training:
Go to the  ```Configs/``` directory, edit the existing ```regress_v2.json``` or duplicate and create yours, and in the json file, provide:
- jets_container: the name of the jet container in your h5 training sample
- tracks_container: the name of the tracks container in your h5 training fample
- targetfeatnames: the name of the variable you want to use as your training target
- jetfeatnames: The names of the input variables to the jet nodes
- trackfeatnames: the names of the input variables to the track nodes
- jetnodes: the shape of the jet nodes (width and depth)
- tracknodes: the shape of the track nodes (width and depth)
- lr: the learning rate
- batch_size: the btaching the training sample that it's divided to before training
- epoch_size: the number of batch iterations inside each epoch
- num_epochs: the number of training iterations that will be executed on your training dataset

3. Run the training
Go to the  ```Notebooks/``` directory, open the ```DIPZ_Training.ipynb``` notebok , and run all the cells which would:
- Define the DIPZ architecture based on your provided information in the config ```regress_v2.json```
- run the training provided a config file + a path to the training dataset that you want the training to be run on (in h5 format)
- Produce plots showing the training/validation performance for different metrics 
- Save the weights of the training, information about the architecture and the inputs, and the performance plots in a timestamped folder inside the ```Outputs/``` directory
**In case you're running on the SMU M3 cluster and want to use the GPUs on the superpod for your training:** 
- Go to the  ```SMU_M3_Scripts/``` directory, edit the ```SuperPOD_submission.sh``` file to add the absolute path to your ```Notebooks/``` directory and your SMU email to receive updates about the training, then you should be able to simply do ```sbatch SuperPOD_submission.sh``` and the script will just submit your training to be run on the superpod GPUs and after it's done, you can open the notebook and find the training results there or check the ```Outputs/``` directory 