#!/bin/bash

#SBATCH -J retraining_DIPZ_superpod #Job name to display in squeue
#SBATCH -o output-retraining_DIPZ.txt    # standard output file
#SBATCH -e error-retraining_DIPZ.txt     # standard error file
#SBATCH -p batch     #requested partition
#SBATCH --mem=500G          # Total memory required
#SBATCH -D /users/maboelela/PhD/DIPZ/The_DIPZ_Project/Training_And_Validation/Notebooks  # sets the working directory where the batch script should be run (CHANGE THIS IF YOU ARE NOT MO)
#SBATCH --gres=gpu:2                 # number of GPUs gpu:1 = 1 gpu, gpu2:2 = 2 gpu
#SBATCH --mail-user maboelela@smu.edu   #tells SLURM your email address if you’d like to receive job-related email notifications (CHANGE THIS IF YOU ARE NOT MO)
#SBATCH --mail-type=all

# load conda and activate environment
module purge
module load conda
conda activate dipz

# load cuda libraries
module load gcc/11.2.0
module load cuda
module load cudnn

# run the notebook
# This command is usually used to convert a notebook to a python script,
# but we can also use it to run the notebook and write the output into 
# the same notebook, so when you open it the output areas are populated
jupyter nbconvert --to notebook --inplace --execute ../Notebooks/DIPZ_Training.ipynb
#time python DIPZ_Training.py -t -e
