#!/bin/bash

#SBATCH -J retraining_DIPZ       # job name to display in squeue
#SBATCH -o output-retraining_DIPZ.txt    # standard output file
#SBATCH -e error-retraining_DIPZ.txt     # standard error file
#SBATCH -p standard-s     # requested partition
#SBATCH --nodes=1                    # number of nodes to run on
#SBATCH --ntasks-per-node=1          # number of tasks per node 
#SBATCH --cpus-per-task=10            # number of cores per task
#SBATCH --mem=100G          # Total memory required
#SBATCH -D /users/maboelela/Research/Qualification_Task/The_DIPZ_Project/DIPZ_Training&Validation  #sets the working directory where the batch script should be run
#SBATCH -s   #tells SLURM that the job can not share nodes with other running jobs
#SBATCH --mail-user maboelela@smu.edu   #tells SLURM your email address if you’d like to receive job-related email notifications
#SBATCH --mail-type=all
#SBATCH --exclusive


# make sure the module system is set up and there are no modules loaded from the environment
module purge

# load the modules we want. 
module load conda

# make sure Conda commands are available in the script
# and load our Conda environment. This is the same thing
# that goes in the custom environment settings on hpc.smu.edu
eval "$(conda shell.bash hook)"
conda activate conda_env_name

# run the notebook
# This command is usually used to convert a notebook to a python script,
# but we can also use it to run the notebook and write the output into 
# the same notebook, so when you open it the output areas are populated
jupyter nbconvert --to notebook --inplace --execute test_notebook.ipynb 
