# functions.py

#Utilities to be used in the mlpl functions
from h5py import File
import numpy as np
import itertools as it
import xml.etree.ElementTree as ET
import time

# A function that takes in the event number and calculates the maximum over 4-jet combinations of log of the product of the likelihood functions of all the jets in the event ANALYTICALLY
def get_MLPL_an(event_id,jets,comb_num,num_highest_pt=555555):
    event_jets = jets[jets["eventNumber"] == event_id]
    
    if num_highest_pt != 555555:
        if len(event_jets) > num_highest_pt:
            event_jets = event_jets[(-event_jets['pt']).argsort()[:num_highest_pt]]
    
    combinations = []
    mlpl_array = []

    for combination in it.combinations(event_jets, comb_num):
        combinations.append(combination)

    for comb in combinations:
        num = 0
        denom = 0
        second_term = 0
        third_term = 0
        
        for jet in comb:
            mu = jet["dipz20231122_z"]  
            sigma = np.exp(-0.5*jet["dipz20231122_negLogSigma2"])  
            num += (mu) / (sigma**2)
            denom += 1 / (sigma**2)
            second_term -= np.log(sigma)
        
        for jet in comb:
            mu = jet["dipz20231122_z"]  
            sigma = np.exp(-0.5*jet["dipz20231122_negLogSigma2"]) 
            third_term -= ((num / denom) - mu)**2 / (2*(sigma**2)) 
            
        mlpl_array.append(- (comb_num/2) * np.log(2*np.pi) + second_term + third_term)
            
    
    max_log_likelihood = max(mlpl_array)
        
    return max_log_likelihood

def get_MLPL_an_old(event_id,jets,comb_num,num_highest_pt=555555):
    event_jets = jets[jets["eventNumber"] == event_id]
    
    if num_highest_pt != 555555:
        if len(event_jets) > num_highest_pt:
            event_jets = event_jets[(-event_jets['pt']).argsort()[:num_highest_pt]]
    
    combinations = []
    mlpl_array = []

    for combination in it.combinations(event_jets, comb_num):
        combinations.append(combination)

    for comb in combinations:
        num = 0
        denom = 0
        second_term = 0
        third_term = 0
        
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50  
            num += (mu) / (sigma**2)
            denom += 1 / (sigma**2)
            second_term -= np.log(sigma)
        
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            third_term -= ((num / denom) - mu)**2 / (2*(sigma**2)) 
            
        mlpl_array.append(- (comb_num/2) * np.log(2*np.pi) + second_term + third_term)
            
    
    max_log_likelihood = max(mlpl_array)
        
    return max_log_likelihood

def get_MLPL_an_hh4b(event_id,jets,comb_num,num_highest_pt=555555):
    event_jets = jets[jets["eventNumber"] == event_id]

    if num_highest_pt != 555555:
        if len(event_jets) > num_highest_pt:
            event_jets = event_jets[(-event_jets['pt']).argsort()[:num_highest_pt]]
    
    combinations = []

    for combination in it.combinations(event_jets, comb_num):
        combinations.append(combination)

    mlpl = -999999999999
    num_bjets_chosen = 0

    for comb in combinations:
        num = 0
        denom = 0
        second_term = 0
        third_term = 0
        num_bjets = 0 
        
        for jet in comb:
            mu = jet["dipz20231122_z"] 
            sigma = np.exp(-0.5*jet["dipz20231122_negLogSigma2"]) 
            num += (mu) / (sigma**2)
            denom += 1 / (sigma**2)
            second_term -= np.log(sigma)
            
        for jet in comb:
            mu = jet["dipz20231122_z"] 
            sigma = np.exp(-0.5*jet["dipz20231122_negLogSigma2"]) 
            third_term -= ((num / denom) - mu)**2 / (2*(sigma**2))

            if jet['HadronConeExclTruthLabelID'] == 5:
                num_bjets += 1
    
        if - (comb_num/2) * np.log(2*np.pi) + second_term + third_term > mlpl:
            mlpl = - (comb_num/2) * np.log(2*np.pi) + second_term + third_term
            num_bjets_chosen = num_bjets
    
    max_log_likelihood = mlpl
    
    return max_log_likelihood, num_bjets_chosen

def get_MLPL_an_hh4b_old(event_id,jets,comb_num,num_highest_pt=555555):
    event_jets = jets[jets["eventNumber"] == event_id]

    if num_highest_pt != 555555:
        if len(event_jets) > num_highest_pt:
            event_jets = event_jets[(-event_jets['pt']).argsort()[:num_highest_pt]]
    
    combinations = []

    for combination in it.combinations(event_jets, comb_num):
        combinations.append(combination)

    mlpl = -999999999999
    num_bjets_chosen = 0

    for comb in combinations:
        num = 0
        denom = 0
        second_term = 0
        third_term = 0
        num_bjets = 0 
        
        for jet in comb:
            mu = jet["dipz20230223_z"]* 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            num += (mu) / (sigma**2)
            denom += 1 / (sigma**2)
            second_term -= np.log(sigma)
            
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            third_term -= ((num / denom) - mu)**2 / (2*(sigma**2))

            if jet['HadronConeExclTruthLabelID'] == 5:
                num_bjets += 1
    
        if - (comb_num/2) * np.log(2*np.pi) + second_term + third_term > mlpl:
            mlpl = - (comb_num/2) * np.log(2*np.pi) + second_term + third_term
            num_bjets_chosen = num_bjets
    
    max_log_likelihood = mlpl
    
    return max_log_likelihood, num_bjets_chosen

# A function that takes in an MC data sample name, runs an algorithm over all the events in the sample, and then outputs:
# 1. a list of the discriminant variable (MLPL) values
# 2. a corresponding list of the MC weights of the events whose MLPL values are computed and stored
def get_MLPL_dist_MC(name, comb_num, num_highest_pt=555555, pt_cut = 20, old_dipz = False):
    start = time.time()
    data = File(name, 'r')
    jets = np.asarray(data['jets'])
    uniques = np.unique(jets["eventNumber"])
        
    max_log_likelihood_list = []
    mlpl_ids = []
    mcEventWeight_list = []
    
    print("The initial number of events in the provided data sample is: " + str(len(uniques)))

    for id in uniques:
        event_jets = jets[jets["eventNumber"] == id]
        event_jets_above_threshold = event_jets[event_jets["pt"] >= pt_cut * 1000]

        if len(event_jets_above_threshold) >= comb_num:
            if old_dipz == True:
                max_log_likelihood_list.append(get_MLPL_an_old(id,jets,comb_num,num_highest_pt))
            else:
                max_log_likelihood_list.append(get_MLPL_an(id,jets,comb_num,num_highest_pt))
            mlpl_ids.append(id)
            mcEventWeight_list.append(jets[jets["eventNumber"] == id][0]['mcEventWeight'])
            if len(np.unique(jets[jets["eventNumber"] == id]['mcEventWeight'])) != 1:
                print('ERROR!!! There is an event ID that has more than one event weight attributed to it')

    print("The number of events passing the DIPZ hypo is in the sample is: " + str(len(max_log_likelihood_list)))
    end = time.time()
    print("The time of execution of the (get_MLPL_dist) function for the (" + name + ") file is :" + str( round((end-start)/60, 2) ) + " min")

    return max_log_likelihood_list, mcEventWeight_list, mlpl_ids

# A function that takes in a HH4b h5 MC sample name, runs an algorithm over all the events in the sample, and then outputs:
# 1. a list of the discriminant variable (MLPL) values
# 2. a corresponding list of the MC weights of the events whose MLPL values are computed and stored
# 3. a corresponding list of the number of the bjets in the chosen combination of the highest maximum log likelihood value
def get_MLPL_dist_hh4b(name, comb_num, num_highest_pt=555555, pt_cut = 20, old_dipz = False):
    start = time.time()
    data = File(name, 'r')
    jets = data['jets']
    jets = np.asarray(jets)
    uniques = np.unique(jets["eventNumber"])
        
    max_log_likelihood_list = []
    mcEventWeight_list = []
    num_bjets_chosen_list = []
    no_of_processed_events = num
    counter = 0

    print("The initial number of events in the provided data sample is: " + str(len(uniques)))

    for id in uniques:
        event_jets = jets[jets["eventNumber"] == id]
        event_jets = event_jets[event_jets["pt"] > pt_cut * 1000]
        bjets = event_jets[event_jets["HadronConeExclTruthLabelID"] == 5]
        
        if len(event_jets) >= comb_num and len(bjets) >= 4:
            if old_dipz == True:
                max_log_likelihood, num_bjets_chosen = get_MLPL_an_hh4b_old(id,jets,comb_num,num_highest_pt)
                max_log_likelihood_list.append(max_log_likelihood)
            else:
                max_log_likelihood, num_bjets_chosen = get_MLPL_an_hh4b(id,jets,comb_num,num_highest_pt)
                max_log_likelihood_list.append(max_log_likelihood)

            mcEventWeight_list.append(jets[jets["eventNumber"] == id][0]['mcEventWeight'])
            if len(np.unique(jets[jets["eventNumber"] == id]['mcEventWeight'])) != 1:
                print('ERROR!!! There is an event ID that has more than one event weight attributed to it')
            num_bjets_chosen_list.append(num_bjets_chosen)
            counter +=1

    if counter != no_of_processed_events:
        print("The number of events passing the DIPZ hypo is in the sample is: " + str(counter))
        print("The number of events passing the DIPZ hypo in the sample is less than the provided number, therefore all the sample was run over.")
    end = time.time()
    print("The time of execution of the (get_MLPL_dist) function is :", ((end-start) / 60) , "min")

    return max_log_likelihood_list, mcEventWeight_list, num_bjets_chosen_list


# A function that takes in am Enhanced Biased data h5 sample name, runs an algorithm over all the events in the sample, and then outputs:
# 1. a list of the discriminant variable (MLPL) values
# 2. a corresponding list of the EB data weights of the events whose MLPL values are computed and stored
def get_MLPL_dist_EBdata(name, xml, comb_num, num_highest_pt=555555, pt_cut = 20, old_dipz = False):
    start = time.time()

    tree = ET.parse(xml)
    root = tree.getroot()
    
    weights = root.find('weights')
    weights_data = {}
    for weight in weights.findall('weight'):
        weight_id = weight.get('id')
        value = weight.get('value')
        unbiased = weight.get('unbiased')
        weights_data[weight_id] = {
            'value': value,
            'unbiased': unbiased
        }
    
    events = root.find('events')
    events_data = {}
    for event in events.findall('e'):
        event_n = event.get('n')
        event_w = event.get('w')
        events_data[event_n] = event_w
    
    data = File(name, 'r')
    jets = np.asarray(data['jets'])
    uniques = np.unique(jets["eventNumber"])
        
    max_log_likelihood_list = []
    mlpl_ids = []
    weights_list = []

    print("The initial number of events in the provided data sample is: " + str(len(uniques)))

    for id in uniques:
        event_jets = jets[jets["eventNumber"] == id]
        event_jets_above_threshold = event_jets[event_jets["pt"] >= pt_cut * 1000]
        
        if len(event_jets_above_threshold) >= comb_num:
            if old_dipz == True:
                max_log_likelihood_list.append(get_MLPL_an_old(id,jets,comb_num,num_highest_pt))
            else:
                max_log_likelihood_list.append(get_MLPL_an(id,jets,comb_num,num_highest_pt))
            weights_list.append(float(weights_data[str(events_data[str(id)])]["value"]))
            mlpl_ids.append(id)


    print("The number of events passing the DIPZ hypo is in the sample is: " + str(len(max_log_likelihood_list)))
    end = time.time()
    print("The time of execution of the (get_MLPL_dist) function for the (" + name + ") file is :" + str( round((end-start)/60, 2) ) + " min")

    return max_log_likelihood_list, weights_list, mlpl_ids


def get_MLPL_dist_MC_all(name, comb_num, num_highest_pt=555555, pt_cut = 20, pflow_jet_number=0, pflow_pt_cut=0, old_dipz = False):
    start = time.time()
    data = File(name, 'r')
    pflow = data['pflow']
    emtopo = data['emtopo']
    pflow_jets = pflow['jets']
    emtopo_jets = emtopo['jets']    
    pflow_jets = np.asarray(pflow_jets)
    emtopo_jets = np.asarray(emtopo_jets)
    uniques_pflow = np.unique(pflow_jets["eventNumber"])
    uniques_emtopo = np.unique(emtopo_jets["eventNumber"])
            
    max_log_likelihood_list = []
    mlpl_ids = []
    mcEventWeight_list = []
    
    print("The initial number of events in the provided data sample is: " + str(len(uniques_emtopo)))

    for id in uniques_emtopo:
        event_jets = emtopo_jets[emtopo_jets["eventNumber"] == id]
        event_jets_above_threshold = event_jets[event_jets["pt"] >= pt_cut * 1000]
        event_EMPFlowjets = pflow_jets[pflow_jets["eventNumber"] == id]
        event_EMPFlowjets_above_threshold = event_EMPFlowjets[event_EMPFlowjets["pt"] >= pflow_pt_cut * 1000]

        if len(event_jets_above_threshold) >= comb_num and len(event_EMPFlowjets_above_threshold) >= pflow_jet_number:
            if old_dipz == True:
                max_log_likelihood_list.append(get_MLPL_an_old(id,emtopo_jets,comb_num,num_highest_pt))
            else:
                max_log_likelihood_list.append(get_MLPL_an(id,emtopo_jets,comb_num,num_highest_pt))
            mlpl_ids.appned(id)
            mcEventWeight_list.append(emtopo_jets[emtopo_jets["eventNumber"] == id][0]['mcEventWeight'])
            if len(np.unique(emtopo_jets[emtopo_jets["eventNumber"] == id]['mcEventWeight'])) != 1:
                print('ERROR!!! There is an event ID that has more than one event weight attributed to it')

    print("The number of events passing the DIPZ hypo is in the sample is: " + str(len(max_log_likelihood_list)))
    end = time.time()
    print("The time of execution of the (get_MLPL_dist) function for the (" + name + ") file is :" + str( round((end-start)/60, 2) ) + " min")

    return max_log_likelihood_list, mlpl_ids, mcEventWeight_list
