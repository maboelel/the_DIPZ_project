#!/usr/bin/env python
# coding: utf-8

"""Importing all needed packages"""
from h5py import File
import numpy as np
import itertools as it
import time
import joblib
from functions import get_max_log_likelihood_an
from functions import get_max_log_likelihood_an_hh4b
from functions import get_max_log_likelihood_dist_JZ
from functions import get_max_log_likelihood_dist_hh4b
from functions import get_max_log_likelihood_dist_EBdata

hypos = [
    #[ '4' , 'all' ],     
    #[ '4' , '6' ],          
    #[ '4' , '5' ],          
    [ '3' , 'all' ],     
    [ '3' , '5' ],         
    #[ '2' , 'all' ],      
]

for hypo in hypos:
    folder_name = './MLPL_Distributions/mlpl_' + hypo[0] + '_' + hypo[1] + '/'
    comb_num = int(hypo[0])
    max_num = 9999999
    if hypo[1] == 'all' :
        num_highest_pt = 555555
    else: 
        num_highest_pt = int(hypo[1])
    
    # Calculate the MLPL distributions for the HH4b signal, JZ0-5 background slices, and the EB data sample
    hh4b_dist, hh4b_dist_weights, num_bjets_chosen_list= get_max_log_likelihood_dist_hh4b("/users/maboelela/PhD/DIPZ/Datasets/hh4b.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(hh4b_dist, folder_name + 'hh4b_dist_nofake.sav')
    joblib.dump(hh4b_dist_weights, folder_name + 'hh4b_dist_weights.sav')
    joblib.dump(num_bjets_chosen_list, folder_name + 'num_bjets_chosen_list.sav')
    """
    jz0_dist, jz0_dist_weights = get_max_log_likelihood_dist_JZ("/users/maboelela/PhD/DIPZ/Datasets/jz0.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(jz0_dist, folder_name + 'jz0_dist.sav')
    joblib.dump(jz0_dist_weights, folder_name + 'jz0_dist_weights.sav')
    jz1_dist, jz1_dist_weights = get_max_log_likelihood_dist_JZ("/users/maboelela/PhD/DIPZ/Datasets/jz1.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(jz1_dist, folder_name + 'jz1_dist.sav')
    joblib.dump(jz1_dist_weights, folder_name + 'jz1_dist_weights.sav')
    jz2_dist, jz2_dist_weights = get_max_log_likelihood_dist_JZ("/users/maboelela/PhD/DIPZ/Datasets/jz2.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(jz2_dist, folder_name + 'jz2_dist.sav')
    joblib.dump(jz2_dist_weights, folder_name + 'jz2_dist_weights.sav')
    jz3_dist, jz3_dist_weights = get_max_log_likelihood_dist_JZ("/users/maboelela/PhD/DIPZ/Datasets/jz3.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(jz3_dist, folder_name + 'jz3_dist.sav')
    joblib.dump(jz3_dist_weights, folder_name + 'jz3_dist_weights.sav')
    jz4_dist, jz4_dist_weights = get_max_log_likelihood_dist_JZ("/users/maboelela/PhD/DIPZ/Datasets/jz4.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(jz4_dist, folder_name + 'jz4_dist.sav')
    joblib.dump(jz4_dist_weights, folder_name + 'jz4_dist_weights.sav')
    jz5_dist, jz5_dist_weights = get_max_log_likelihood_dist_JZ("/users/maboelela/PhD/DIPZ/Datasets/jz5.h5", comb_num, max_num, num_highest_pt)
    joblib.dump(jz5_dist, folder_name + 'jz5_dist.sav')
    joblib.dump(jz5_dist_weights, folder_name + 'jz5_dist_weights.sav')
    """
    EB_dist, EB_dist_weights = get_max_log_likelihood_dist_EBdata("/users/maboelela/PhD/DIPZ/Datasets/EBdata.h5", "/users/maboelela/PhD/DIPZ/Datasets/EnhancedBiasWeights_440499.xml", comb_num, max_num, num_highest_pt)
    joblib.dump(EB_dist, folder_name + 'EB_dist.sav')
    joblib.dump(EB_dist_weights, folder_name + 'EB_dist_weights.sav')