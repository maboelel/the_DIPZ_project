#!/usr/bin/env python
# coding: utf-8
from h5py import File
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.stats import norm
import random
import itertools as it
import time
import joblib

start = time.time()

def get_dists(string, max_num=60000):
    data = File(string, 'r')
    jets = data['jets']
    jets = np.asarray(jets)
    uniques = np.unique(jets["eventNumber"])
        
    counter = 0
    jet_multiplicities = []
    leading_pT = []
    subleading_pT = []
    mc_weights = []
    

    for i in uniques:
        jet_multiplicities.append(len(jets[jets["eventNumber"] == i]))
        pTs = []
        event_jets = jets[jets["eventNumber"] == i]
        for jet in event_jets:
            pTs.append(jet["pt"])
        leading_pT.append(max(pTs))
        if len(pTs) > 1: 
            subleading_pT.append(sorted(pTs)[-2])
        mc_weights.append(jets[jets["eventNumber"] == i][0]['mcEventWeight'])
        if len(np.unique(jets[jets["eventNumber"] == i]['mcEventWeight'])) != 1:
            print('ERROR!!! There is an event ID that has more than one event weight attributed to it')
        counter += 1
        if counter == max_num:
            break 
    return jet_multiplicities, leading_pT, subleading_pT, mc_weights


#hh4b_multiplicities, hh4b_leading_pt_dist, hh4b_subleading_pt_dist, hh4b_mc_weights = get_dists('../../../Datasets/hh4b.h5')
#joblib.dump(hh4b_multiplicities, './Jet_Multiplicity_Distributions/hh4b_multiplicities.sav')
#joblib.dump(hh4b_leading_pt_dist, './Jet_Leading_pt_Distributions/hh4b_leading_pt_dist.sav')
#joblib.dump(hh4b_subleading_pt_dist, './Jet_Subleading_pt_Distributions/hh4b_subleading_pt_dist.sav')
#joblib.dump(hh4b_mc_weights, './MC_weights/hh4b_mc_weights.sav')


#EBdata_multiplicities, EBdata_leading_pt_dist, EBdata_subleading_pt_dist = get_dists('../../../Datasets/EBdata.h5')
#joblib.dump(EBdata_multiplicities, './Jet_Multiplicity_Distributions/EBdata_multiplicities.sav')
#joblib.dump(EBdata_leading_pt_dist, './Jet_Leading_pt_Distributions/EBdata_leading_pt_dist.sav')
#joblib.dump(EBdata_subleading_pt_dist, './Jet_Subleading_pt_Distributions/EBdata_subleading_pt_dist.sav')


jz0_multiplicities, jz0_leading_pt_dist, jz0_subleading_pt_dist, jz0_mc_weights = get_dists('../../../Datasets/jz0.h5')
joblib.dump(jz0_multiplicities, './Jet_Multiplicity_Distributions/jz0_multiplicities.sav')
joblib.dump(jz0_leading_pt_dist, './Jet_Leading_pt_Distributions/jz0_leading_pt_dist.sav')
joblib.dump(jz0_subleading_pt_dist, './Jet_Subleading_pt_Distributions/jz0_subleading_pt_dist.sav')
joblib.dump(jz0_mc_weights, './MC_weights/jz0_mc_weights.sav')




jz1_multiplicities, jz1_leading_pt_dist, jz1_subleading_pt_dist, jz1_mc_weights = get_dists('../../../Datasets/jz1.h5')
joblib.dump(jz1_multiplicities, './Jet_Multiplicity_Distributions/jz1_multiplicities.sav')
joblib.dump(jz1_leading_pt_dist, './Jet_Leading_pt_Distributions/jz1_leading_pt_dist.sav')
joblib.dump(jz1_subleading_pt_dist, './Jet_Subleading_pt_Distributions/jz1_subleading_pt_dist.sav')
joblib.dump(jz1_mc_weights, './MC_weights/jz1_mc_weights.sav')


jz2_multiplicities, jz2_leading_pt_dist, jz2_subleading_pt_dist, jz2_mc_weights = get_dists('../../../Datasets/jz2.h5')
joblib.dump(jz2_multiplicities, './Jet_Multiplicity_Distributions/jz2_multiplicities.sav')
joblib.dump(jz2_leading_pt_dist, './Jet_Leading_pt_Distributions/jz2_leading_pt_dist.sav')
joblib.dump(jz2_subleading_pt_dist, './Jet_Subleading_pt_Distributions/jz2_subleading_pt_dist.sav')
joblib.dump(jz2_mc_weights, './MC_weights/jz2_mc_weights.sav')


jz3_multiplicities, jz3_leading_pt_dist, jz3_subleading_pt_dist, jz3_mc_weights = get_dists('../../../Datasets/jz3.h5')
joblib.dump(jz3_multiplicities, './Jet_Multiplicity_Distributions/jz3_multiplicities.sav')
joblib.dump(jz3_leading_pt_dist, './Jet_Leading_pt_Distributions/jz3_leading_pt_dist.sav')
joblib.dump(jz3_subleading_pt_dist, './Jet_Subleading_pt_Distributions/jz3_subleading_pt_dist.sav')
joblib.dump(jz3_mc_weights, './MC_weights/jz3_mc_weights.sav')


jz4_multiplicities, jz4_leading_pt_dist, jz4_subleading_pt_dist, jz4_mc_weights = get_dists('../../../Datasets/jz4.h5')
joblib.dump(jz4_multiplicities, './Jet_Multiplicity_Distributions/jz4_multiplicities.sav')
joblib.dump(jz4_leading_pt_dist, './Jet_Leading_pt_Distributions/jz4_leading_pt_dist.sav')
joblib.dump(jz4_subleading_pt_dist, './Jet_Subleading_pt_Distributions/jz4_subleading_pt_dist.sav')
joblib.dump(jz4_mc_weights, './MC_weights/jz4_mc_weights.sav')


jz5_multiplicities, jz5_leading_pt_dist, jz5_subleading_pt_dist, jz5_mc_weights = get_dists('../../../Datasets/jz5.h5')
joblib.dump(jz5_multiplicities, './Jet_Multiplicity_Distributions/jz5_multiplicities.sav')
joblib.dump(jz5_leading_pt_dist, './Jet_Leading_pt_Distributions/jz5_leading_pt_dist.sav')
joblib.dump(jz5_subleading_pt_dist, './Jet_Subleading_pt_Distributions/jz5_subleading_pt_dist.sav')
joblib.dump(jz5_mc_weights, './MC_weights/jz5_mc_weights.sav')

finish = time.time()

print("The time of execution of the (get_nonMLPL_dists.py) is:", round(finish-start / 60, 2) , "min")