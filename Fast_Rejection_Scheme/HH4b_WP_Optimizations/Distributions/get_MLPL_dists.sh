#!/bin/bash

#SBATCH -J get_MLPL_dists       # job name to display in squeue
#SBATCH -p standard-s     # requested partition
#SBATCH --nodes=1                    # number of nodes to run on
#SBATCH --ntasks-per-node=1          # number of tasks per node 
#SBATCH --cpus-per-task=25            # number of cores per task
#SBATCH --mem=100G          # Total memory required
#SBATCH -D /users/maboelela/DIPZ_Repo/Fast_Rejection_Scheme/HH4b_WP_Optimizations/Distributions  #sets the working directory where the batch script should be run
#SBATCH -s   #tells SLURM that the job can not share nodes with other running jobs
#SBATCH --mail-user maboelela@smu.edu   #tells SLURM your email address if you’d like to receive job-related email notifications
#SBATCH --mail-type=all


module purge
module load conda
eval "$(conda shell.bash hook)"
conda activate /lustre/work/client/users/maboelela/.conda/envs/dipz


# run the notebook
# This command is usually used to convert a notebook to a python script,
# but we can also use it to run the notebook and write the output into 
# the same notebook, so when you open it the output areas are populated
time python get_MLPL_dists.py -t -e