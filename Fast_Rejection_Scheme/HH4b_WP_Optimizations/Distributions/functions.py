#Utilities to be used in the mlpl functions
from h5py import File
import numpy as np
import itertools as it
import xml.etree.ElementTree as ET
import time

# A function that takes in the event number and calculates the maximum over 4-jet combinations of log of the product of the likelihood functions of all the jets in the event ANALYTICALLY
def get_max_log_likelihood_an(event_id,jets,comb_num,num_highest_pt=555555):
    event_jets = jets[jets["eventNumber"] == event_id]
    
    if num_highest_pt != 555555:
        if len(event_jets) > num_highest_pt:
            event_jets = event_jets[(-event_jets['pt']).argsort()[:num_highest_pt]]
    
    combinations = []
    mlpl_array = []

    for combination in it.combinations(event_jets, comb_num):
        combinations.append(combination)

    for comb in combinations:
        num = 0
        denom = 0
        second_term = 0
        third_term = 0
        
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            num += (mu) / (sigma**2)
            denom += 1 / (sigma**2)
            second_term -= np.log(sigma)
        
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            third_term -= ((num / denom) - mu)**2 / (2*(sigma**2)) 
            
        mlpl_array.append(- (comb_num/2) * np.log(2*np.pi) + second_term + third_term)
            
    
    max_log_likelihood = max(mlpl_array)
        
    return max_log_likelihood

# Same function but with the added utility of counting the number of b-jets in the chosen jet combination for the HH4b samples
def get_max_log_likelihood_an_hh4b(event_id,jets,comb_num,num_highest_pt=555555):
    event_jets = jets[jets["eventNumber"] == event_id]

    if num_highest_pt != 555555:
        if len(event_jets) > num_highest_pt:
            event_jets = event_jets[(-event_jets['pt']).argsort()[:num_highest_pt]]
    
    combinations = []

    for combination in it.combinations(event_jets, comb_num):
        combinations.append(combination)

    mlpl = -999999999999
    num_bjets_chosen = 0

    for comb in combinations:
        num = 0
        denom = 0
        second_term = 0
        third_term = 0
        num_bjets = 0 
        
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            num += (mu) / (sigma**2)
            denom += 1 / (sigma**2)
            second_term -= np.log(sigma)
            
        for jet in comb:
            mu = jet["dipz20230223_z"] * 50
            sigma = np.exp(-0.5*jet["dipz20230223_negLogSigma2"]) * 50
            third_term -= ((num / denom) - mu)**2 / (2*(sigma**2))

            if jet['HadronConeExclTruthLabelID'] == 5:
                num_bjets += 1
    
        if - (comb_num/2) * np.log(2*np.pi) + second_term + third_term > mlpl:
            mlpl = - (comb_num/2) * np.log(2*np.pi) + second_term + third_term
            num_bjets_chosen = num_bjets
    
    max_log_likelihood = mlpl
    
    return max_log_likelihood, num_bjets_chosen

# A function that takes in a JZ h5 MC sample name, runs an algorithm over all the events in the sample, and then outputs:
# 1. a list of the discriminant variable (MLPL) values
# 2. a corresponding list of the MC weights of the events whose MLPL values are computed and stored
def get_max_log_likelihood_dist_JZ(name, comb_num, num=999999999999999 ,num_highest_pt=555555):
    start = time.time()
    data = File(name, 'r')
    jets = data['jets']    
    jets = np.asarray(jets)
    uniques = np.unique(jets["eventNumber"])
    
    print("The number of jets in the sample is: " + str(len(jets)))
    print("The number of jets in the sample with pT < 20 GeV is: " + str(len(jets[jets["pt"] < 20000])))
    print("The number of jets in the sample with eta > 2.5 GeV is: " + str(len(jets[jets["eta"] > 2.5])))
    print("The number of events in our sample is: " + str(len(uniques)))
    
    max_log_likelihood_list = []
    mcEventWeight_list = []
    no_of_processed_events = num
    counter = 0

    for id in uniques:
        
        # Emulating the HH4b L1 seed (L1jJ85p0ETA21_3jJ40p0ETA25)
        event_jets = jets[jets["eventNumber"] == id]
        if len(event_jets[abs(event_jets['eta']) < 2.1][event_jets[abs(event_jets['eta']) < 2.1]['pt'] > 85000]) >= 1 and len(event_jets[abs(event_jets['eta']) < 2.5][event_jets[abs(event_jets['eta']) < 2.5]['pt'] > 40000]) >= 3:
            if len(jets[jets["eventNumber"] == id]) >= comb_num:
                max_log_likelihood_list.append(get_max_log_likelihood_an(id,jets,comb_num,num_highest_pt))
                mcEventWeight_list.append(jets[jets["eventNumber"] == id][0]['mcEventWeight'])
                if len(np.unique(jets[jets["eventNumber"] == id]['mcEventWeight'])) != 1:
                    print('ERROR!!! There is an event ID that has more than one event weight attributed to it')
                counter +=1

    if counter != no_of_processed_events:
        print("The number of four or more jet events in the sample is: " + str(counter))
        print("The number of four or more jet events in the sample is less than the provided number, therefore all the sample was run over.")
    end = time.time()
    print("The time of execution of the (get_max_log_likelihood_dist) function for the (" + name + ") file is :", ((end-start) / 60) , "min")

    return max_log_likelihood_list, mcEventWeight_list

# A function that takes in a HH4b h5 MC sample name, runs an algorithm over all the events in the sample, and then outputs:
# 1. a list of the discriminant variable (MLPL) values
# 2. a corresponding list of the MC weights of the events whose MLPL values are computed and stored
# 3. a corresponding list of the number of the bjets in the chosen combination of the highest maximum log likelihood value
def get_max_log_likelihood_dist_hh4b(name, comb_num, num=999999999999999, num_highest_pt=555555):
    start = time.time()
    data = File(name, 'r')
    jets = data['jets']
    jets = np.asarray(jets)
    uniques = np.unique(jets["eventNumber"])
    
    print("The number of jets in the sample is: " + str(len(jets)))
    print("The number of jets in the sample with pT < 20 GeV is: " + str(len(jets[jets["pt"] < 20])))
    print("The number of jets in the sample with eta > 2.5 GeV is: " + str(len(jets[jets["eta"] > 2.5])))
    print("The number of events in our sample is: " + str(len(uniques)))
    
    max_log_likelihood_list = []
    mcEventWeight_list = []
    num_bjets_chosen_list = []
    no_of_processed_events = num
    counter = 0

    for id in uniques:
        # Emulating the HH4b L1 seed (L1jJ85p0ETA21_3jJ40p0ETA25)
        event_jets = jets[jets["eventNumber"] == id]
        if len(event_jets[abs(event_jets['eta']) < 2.1][event_jets[abs(event_jets['eta']) < 2.1]['pt'] > 85000]) >= 1 and len(event_jets[abs(event_jets['eta']) < 2.5][event_jets[abs(event_jets['eta']) < 2.5]['pt'] > 40000]) >= 3:
            bjets = event_jets[event_jets["HadronConeExclTruthLabelID"] == 5]
            if len(event_jets) >= comb_num and len(bjets) >= 4:
                max_log_likelihood, num_bjets_chosen = get_max_log_likelihood_an_hh4b(id,jets,comb_num,num_highest_pt)
                max_log_likelihood_list.append(max_log_likelihood)
                mcEventWeight_list.append(jets[jets["eventNumber"] == id][0]['mcEventWeight'])
                if len(np.unique(jets[jets["eventNumber"] == id]['mcEventWeight'])) != 1:
                    print('ERROR!!! There is an event ID that has more than one event weight attributed to it')
                num_bjets_chosen_list.append(num_bjets_chosen)
                counter +=1

    if counter != no_of_processed_events:
        print("The number of four or more b-jet events in the sample is: " + str(counter))
        print("The number of four or more b-jet events in the sample is less than the provided number, therefore all the sample was run over.")
    end = time.time()
    print("The time of execution of the (get_max_log_likelihood_dist) function is :", ((end-start) / 60) , "min")

    return max_log_likelihood_list, mcEventWeight_list, num_bjets_chosen_list


# A function that takes in am Enhanced Biased data h5 sample name, runs an algorithm over all the events in the sample, and then outputs:
# 1. a list of the discriminant variable (MLPL) values
# 2. a corresponding list of the EB data weights of the events whose MLPL values are computed and stored
def get_max_log_likelihood_dist_EBdata(name, xml, comb_num, num=999999999999999 ,num_highest_pt=555555):
    start = time.time()

    tree = ET.parse(xml)
    root = tree.getroot()
    
    weights = root.find('weights')
    weights_data = {}
    for weight in weights.findall('weight'):
        weight_id = weight.get('id')
        value = weight.get('value')
        unbiased = weight.get('unbiased')
        weights_data[weight_id] = {
            'value': value,
            'unbiased': unbiased
        }
    
    events = root.find('events')
    events_data = {}
    for event in events.findall('e'):
        event_n = event.get('n')
        event_w = event.get('w')
        events_data[event_n] = event_w
    
    data = File(name, 'r')
    jets = data['jets']
    jets = np.asarray(jets)
    uniques = np.unique(jets["eventNumber"])
    
    print("The number of jets in the sample is: " + str(len(jets)))
    print("The number of jets in the sample with pT < 20 GeV is: " + str(len(jets[jets["pt"] < 20])))
    print("The number of jets in the sample with eta > 2.5 GeV is: " + str(len(jets[jets["eta"] > 2.5])))
    print("The number of events in our sample is: " + str(len(uniques)))
    
    max_log_likelihood_list = []
    weights_list = []
    no_of_processed_events = num
    counter = 0

    for id in uniques:
        # Emulating the HH4b L1 seed (L1jJ85p0ETA21_3jJ40p0ETA25)
        event_jets = jets[jets["eventNumber"] == id]
        if len(event_jets[abs(event_jets['eta']) < 2.1][event_jets[abs(event_jets['eta']) < 2.1]['pt'] > 85000]) >= 1 and len(event_jets[abs(event_jets['eta']) < 2.5][event_jets[abs(event_jets['eta']) < 2.5]['pt'] > 40000]) >= 3:
            if len(jets[jets["eventNumber"] == id]) >= comb_num:
                max_log_likelihood_list.append(get_max_log_likelihood_an(id,jets,comb_num,num_highest_pt))
                weights_list.append(float(weights_data[str(events_data[str(id)])]["value"]))
                counter +=1

    if counter != no_of_processed_events:
        print("The number of four or more jet events in the sample is: " + str(counter))
        print("The number of four or more jet events in the sample is less than the provided number, therefore all the sample was run over.")
    end = time.time()
    print("The time of execution of the (gEet_max_log_likelihood_dist) function for the (" + name + ") file is :", ((end-start) / 60) , "min")

    return max_log_likelihood_list, weights_list