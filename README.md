# DIPZ Workflow

This is a repository that maintains version control for the DIPZ Project

For more details and information about DIPZ, please consult: https://cds.cern.ch/record/2875273/

For an questions or comments, please contact: mo.abdellrazekk@cern.ch
